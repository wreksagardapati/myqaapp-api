# My Question Answer Application (Lumen PHP Framework)

[![Build Status](https://travis-ci.org/laravel/lumen-framework.svg)](https://travis-ci.org/laravel/lumen-framework)
[![Total Downloads](https://poser.pugx.org/laravel/lumen-framework/d/total.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/lumen-framework/v/stable.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/lumen-framework/v/unstable.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![License](https://poser.pugx.org/laravel/lumen-framework/license.svg)](https://packagist.org/packages/laravel/lumen-framework)

Laravel Lumen is a stunningly fast PHP micro-framework for building web applications with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Lumen attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as routing, database abstraction, queueing, and caching.

## Official Documentation

Documentation for the framework can be found on the [Lumen website](https://lumen.laravel.com/docs).

plugins: 
- Lumen Route List Display (https://github.com/appzcoder/lumen-route-list)

Route list:

<table class="tg">
  <tr>
    <td class="tg-yw4l">POST</td>
    <td class="tg-yw4l">/createuser</td>
    <td class="tg-yw4l">create new user</td>
  </tr>
  <tr>
    <td class="tg-yw4l">POST</td>
    <td class="tg-yw4l">/createquestion</td>
    <td class="tg-yw4l">create new question by user</td>
  </tr>
  <tr>
    <td>POST</td>
    <td>/submitanswer</td>
    <td>submit new answer by user and question</td>
  </tr>
  <tr>
    <td>POST</td>
    <td>/userlogin</td>
    <td>login user</td>
  </tr>
  <tr>
    <td>POST</td>
    <td>/createcategory</td>
    <td>create new category</td>
  </tr>
  <tr>
    <td>GET</td>
    <td>/categories</td>
    <td>show all categories</td>
  </tr>
  <tr>
    <td>GET</td>
    <td>/getmyquestions/{user_id}</td>
    <td>show questions by user </td>
  </tr>
  <tr>
    <td>GET</td>
    <td>/questions</td>
    <td>show all of questions</td>
  </tr>
  <tr>
    <td>GET</td>
    <td>/questions/{category_id}</td>
    <td>show questions by category</td>
  </tr>
  <tr>
    <td>GET</td>
    <td>/getanswers/{answer_id}</td>
    <td>show answers by question</td>
  </tr>
</table>




## Security Vulnerabilities

If you discover a security vulnerability within Lumen, please send an e-mail to Taylor Otwell at taylor@laravel.com. All security vulnerabilities will be promptly addressed.

## License

The Lumen framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
